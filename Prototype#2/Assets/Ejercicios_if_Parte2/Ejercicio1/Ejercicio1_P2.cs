﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio1_P2 : MonoBehaviour
{
    public int a = 162;
    // Start is called before the first frame update
    void Start()
    {
        if (a < 135)
            Debug.Log("Muy bajo");
        else if (a >= 135 && a <= 150)
            Debug.Log("Bajo");
        else if (a > 150 && a <= 165)
            Debug.Log("Medio");
        else if (a > 165 && a <= 190)
            Debug.Log("Alto");
        else
            Debug.Log("Muy Alto");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
