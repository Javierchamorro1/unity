﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio3_p2 : MonoBehaviour
{
    public int x = 1;
    public int y = 2;
    // Start is called before the first frame update
    void Start()
    {
        if (x > 0 && y > 0)
            Debug.Log("Esta en el primer cuadrante");
        else if (x < 0 && y > 0)
            Debug.Log("Esta en el segundo cuadrante");
        else if (x < 0 && y < 0)
            Debug.Log("Esta en el tercer cuadrante");
        else if (x > 0 && y < 0)
            Debug.Log("Esta en el cuarto cuadrante");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
