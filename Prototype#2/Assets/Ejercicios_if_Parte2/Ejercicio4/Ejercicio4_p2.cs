﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio4_p2 : MonoBehaviour
{
    public int angulo1 = 50;
    public int angulo2 = 30;
    public int angulo3 = 20;
    // Start is called before the first frame update
    void Start()
    {
        if (angulo1 == angulo2 && angulo1 == angulo3)
            Debug.Log("Es equilatero");
        else if (angulo1 == angulo2 || angulo1 == angulo3 || angulo2 == angulo3)
            Debug.Log("Es Isosceles");
        else
            Debug.Log("Es escaleno");

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
