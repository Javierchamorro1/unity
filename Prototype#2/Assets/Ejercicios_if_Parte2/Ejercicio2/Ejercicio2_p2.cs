﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio2_p2 : MonoBehaviour
{
    public int a = 184;
    public int b = 83;
    public int c = 10;
    // Start is called before the first frame update
    void Start()
    {
        if (a > b && a > c)
            Debug.Log("El primer numero es el mayor");
        else if (b > a && b > c)
            Debug.Log("El segundo numero es el mayor");
        else if (c > a && c > b)
            Debug.Log("El tercer numero es el mayor");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
