﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio3 : MonoBehaviour
{
    public int n =2020;
    // Start is called before the first frame update
    void Start()
    {
        if ((n % 4 == 0) && ((n % 100 != 0) || (n % 400 == 0)))
            Debug.Log("The year " + n + " is leap");

        else
            Debug.Log("The year " + n + " is not leap");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
