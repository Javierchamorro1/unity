﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio5 : MonoBehaviour
{
    public int n = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (n == 0)
            Debug.Log("0");
        else if (n > 0)
            Debug.Log("1");
        else
            Debug.Log("-1");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
