﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio2 : MonoBehaviour
{
    public int n = 5;
    // Start is called before the first frame update
    void Start()
    {
        if (n > 0)
            Debug.Log("The number " + n + " is positive");
        else if (n == 0)
            Debug.Log("The number is 0");
        else
            Debug.Log("The number " + n + " is negative");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
