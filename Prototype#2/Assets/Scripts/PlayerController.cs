﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    public float xRange = 10;
    public float zRange = 0.5f;
    public float speed = 10.0f;
    private float count = 0;
    public GameObject projectilePrefab;
    public GameObject projectilePrefab2;

    private float horizontalInput;
    private float verticalInput;
    Vector3 currentOffset;
    // Start is called before the first frame update
    void Start()
    {
        currentOffset = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (count % 2 == 0)
            {
                Instantiate(projectilePrefab, transform.position, projectilePrefab.transform.rotation);
            }
            else
            {
                Instantiate(projectilePrefab2, transform.position, projectilePrefab2.transform.rotation);
            }
            count++;
        }

        verticalInput = Input.GetAxis("Vertical");
        transform.Translate(Vector3.forward * speed * Time.deltaTime * verticalInput);


        horizontalInput = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * speed * Time.deltaTime * horizontalInput);

        //entra si me salgo por la izquierda
        if (transform.position.x < currentOffset.x - xRange)
        {
            transform.position = new Vector3(currentOffset.x - xRange, transform.position.y, transform.position.z);
        }
        //entra si me salgo por la derecha
        if (transform.position.x > currentOffset.x + xRange)
        {
            transform.position = new Vector3(currentOffset.x + xRange, transform.position.y, transform.position.z);
        }
        //entra si me salgo por detras
        if (transform.position.z < currentOffset.z - zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, currentOffset.z - zRange);
        }
        //entra si me salgo por delante
        if (transform.position.z > currentOffset.z + zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, currentOffset.z + zRange);
        }
    }
}
